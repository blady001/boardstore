//
//  OrderHistoryViewController.swift
//  boardstore
//
//  Created by MacBlady on 10.06.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit
import RealmSwift

class OrderHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let placeholders = ["Sample", "placeholder", "69"]
    
    @IBOutlet weak var ordersTableView: UITableView!
    var orders = [Order]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadOrders()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellOrder", for: indexPath)
        cell.textLabel?.text = "Order - \(orders[indexPath.row].totalAmount)$"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueToOrderDetailsView", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "segueToOrderDetailsView") {
            let destinationController = segue.destination as! OrderDetailsViewController
            destinationController.order = orders[self.ordersTableView.indexPathForSelectedRow!.row]
        }
    }
    
    func loadOrders() {
        orders.removeAll()
        DispatchQueue.main.async {
            let realm = try! Realm()
            let foundOrders = realm.objects(Order.self)
            for order in foundOrders {
                self.orders.append(order)
            }
            self.ordersTableView.reloadData()
            print("Loaded orders count: \(foundOrders.count)")
        }
    }
}
