//
//  SecondViewController.swift
//  boardstore
//
//  Created by MacBlady on 28.04.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var categories = [Category]()
    let categoryUrl = "http://localhost:8080/categories"
    @IBOutlet weak var categoriesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        loadCategories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func parseCategories(categoriesArray: [[String: AnyObject]]) {
        categories.removeAll()
        
        for category in categoriesArray {
            let c = Category()
            c.name = category["name"] as! String
            c.categoryDescription = category["description"] as! String
            categories.append(c)
        }
    }
    
    func loadCategories() {
        let url = URL(string: self.categoryUrl)
        let session = URLSession.shared
        let request = URLRequest(url: url!)
        session.dataTask(with: request, completionHandler: {(data, response, error) in
            guard let data = data, error == nil else { print("[ERROR] Something went wrong while downloading..."); return }
            
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSArray
                //                let posts = json["posts"] as? [[String: Any]] ?? []
                // print(json)
                self.parseCategories(categoriesArray: json as! [[String: AnyObject]])
                DispatchQueue.main.async {
                    self.categoriesTableView.reloadData()
                }
                
            } catch let error as NSError {
                print("Another error:")
                print(error)
            }
        }).resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = categoriesTableView.dequeueReusableCell(withIdentifier: "cellCategory", for: indexPath)
        let category = categories[indexPath.row]
        cell.textLabel?.text = category.name.capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if let t = cell?.textLabel?.text {
            print(t)
            performSegue(withIdentifier: "segueToCategoryOfProducts", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "segueToCategoryOfProducts") {
            let destinationController = segue.destination as! FilteredProductsTableViewController
            destinationController.categoryName = self.categories[self.categoriesTableView.indexPathForSelectedRow!.row].name
        }
    }

}

