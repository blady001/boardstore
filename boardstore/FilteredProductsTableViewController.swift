//
//  FilteredProductsTableViewController.swift
//  boardstore
//
//  Created by MacBlady on 07.06.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit
import RealmSwift

class FilteredProductsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var productsTableView: UITableView!
    
    var products = [Product]()
    
    var categoryName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Received name: \(categoryName!)")
        self.title = "\(categoryName!.capitalized)s"
        loadProducts()
        
    }
    
    func loadProducts() {
        DispatchQueue.main.async {
            let realm = try! Realm()
            let foundProducts = realm.objects(Product.self).filter("category == '\(self.categoryName!)'")
            for p in foundProducts {
                self.products.append(p)
            }
            self.productsTableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return products.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFilteredProduct", for: indexPath)
        
        let p = products[indexPath.row]
        cell.textLabel?.text = p.name

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueFromCategoryToProduct", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "segueFromCategoryToProduct") {
            let destinationController = segue.destination as! ProductViewController
            destinationController.product = products[self.productsTableView.indexPathForSelectedRow!.row]
        }
    }

}
