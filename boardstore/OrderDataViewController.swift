//
//  OrderDataViewController.swift
//  boardstore
//
//  Created by MacBlady on 08.06.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit

class OrderDataViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    
    let paymentMethods = ["Payment on delivery", "Bank transfer"]
    
    
    @IBOutlet weak var paymentTextField: UITextField!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    
    var cartItems: [CartItem]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupPicker()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Proceed", style: .plain, target: self, action: #selector(proceedHandler))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func constructAddress() -> String {
        let firstname = self.firstnameTextField.text!
        let lastname = self.lastnameTextField.text!
        let address = self.addressTextField.text!
        let city = self.cityTextField.text!
        let zip = self.zipTextField.text!
        let country = self.countryTextField.text!
        return "\(firstname) \(lastname)\n\(address)\n\(zip)\(city)\n\(country)"
    }
    
    func calculateTotal(order: Order) {
        var total = Decimal(string: "0.00")
        for ci in order.cartItems {
            var value = Decimal(string: ci.product!.price)
            value = value! * Decimal(ci.quantity)
            total = total! + value!
        }
        let fmt = NumberFormatter()
        fmt.maximumFractionDigits = 2
        fmt.minimumFractionDigits = 2
        let output = fmt.string(from: total! as NSNumber)!
        order.totalAmount = output
    }
    
    func createOrder() -> Order {
        let order = Order()
        order.cartItems.append(objectsIn: cartItems!)
        order.address = constructAddress()
        order.addresseeName = "\(self.firstnameTextField.text!) \(self.lastnameTextField.text!)"
        order.paymentMethod = self.paymentTextField.text!
        calculateTotal(order: order)
        return order
    }
    
    func proceedHandler() {
        // TODO: Apply some verification if all data is passed properly
        performSegue(withIdentifier: "segueToConfirmationView", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "segueToConfirmationView") {
            let destinationController = segue.destination as! ConfirmationViewController
            destinationController.order = createOrder()
        }
    }
    
    func setupPicker() {
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        pickerView.showsSelectionIndicator = true
        pickerView.dataSource = self
        pickerView.delegate = self
        
        paymentTextField.inputView = pickerView
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(pickerDone))
        
        toolbar.setItems([doneButton], animated: true)
        toolbar.isUserInteractionEnabled = true
        
        paymentTextField.inputView = pickerView
        paymentTextField.inputAccessoryView = toolbar
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return paymentMethods[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return paymentMethods.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        paymentTextField.text = paymentMethods[row]
    }
    
    func pickerDone(sender: UIBarButtonItem) {
        paymentTextField.resignFirstResponder()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
