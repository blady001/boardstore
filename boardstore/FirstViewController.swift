//
//  FirstViewController.swift
//  boardstore
//
//  Created by MacBlady on 28.04.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit
import RealmSwift

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var products = [Product]()
    let productsUrl = "http://localhost:8080/products"
    
    @IBOutlet weak var productsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        refreshProducts()
        
        loadProducts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = productsTableView.dequeueReusableCell(withIdentifier: "cellProduct", for: indexPath)
        let product = products[indexPath.row]
        cell.textLabel?.text = product.name
        return cell
    }
    
//    func refreshProducts() {
//        let path = Bundle.main.path(forResource: "sampleProducts", ofType: "json")
//        let jsonData = try? NSData(contentsOfFile: path!, options: NSData.ReadingOptions.mappedIfSafe)
//        // print(jsonData!)
//        let jsonResult: NSDictionary = try! JSONSerialization.jsonObject(with: jsonData! as Data, options: .mutableContainers) as! NSDictionary
//        
//        let productsArray = jsonResult.value(forKey: "products") as! [[String: AnyObject]]
//        
//        products.removeAll()
//        
//        for product in productsArray {
//            let p = Product()
//            p.name = product["name"] as! String
//            p.productDescription = product["description"] as! String
//            p.category = product["category"] as! String
//            p.amount = product["amount"] as! Int
//            p.price = product["price"] as! String
//            products.append(p)
//        }
//    }
    
    func parseProducts(productsArray: [[String: AnyObject]]) -> [Product] {
        var products = [Product]()
        
        for product in productsArray {
            let p = Product()
            p.id = (product["id"] as! NSString).integerValue
            p.name = product["title"] as! String
            p.productDescription = product["description"] as! String
            p.category = product["category"] as! String
            p.amount = (product["amount"] as! NSString).integerValue
            p.price = product["price"] as! String
            p.imageUrl = product["imageurl"] as! String
            products.append(p)
        }
        return products;
    }
    
    func updateRealm(productsFromServer: [Product]) {
        let realm = try! Realm()
        let realmProducts = realm.objects(Product.self)
        
        var toAdd = [Product]()
        var toDelete = [Product]()
        
        for p in productsFromServer {
            if realmProducts.filter("name == '\(p.name)'").count < 1 {
                toAdd.append(p)
            }
        }
        
        var currentProductNames = [String]()
        for p in productsFromServer {
            currentProductNames.append(p.name)
        }
        
        for p in realmProducts {
            if !currentProductNames.contains(p.name) {
                toDelete.append(p)
            }
        }
        
        if toAdd.count > 0 || toDelete.count > 0 {
            try! realm.write {
                for newProduct in toAdd {
                    realm.add(newProduct)
                }
                for deletedProduct in toDelete {
                    realm.delete(deletedProduct)
                }
            }
        }
        
        print("Added \(toAdd.count) products to realm!")
        print("Deleted \(toDelete.count) products from realm!")
    }
    
    
    func loadProducts() {
        let url = URL(string: self.productsUrl)
        let session = URLSession.shared
        let request = URLRequest(url: url!)
        session.dataTask(with: request, completionHandler: {(data, response, error) in
            guard let data = data, error == nil else { print("[ERROR] Something went wrong while downloading..."); return }
            

            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSArray
                //                let posts = json["posts"] as? [[String: Any]] ?? []
                // print(json)
                self.products = self.parseProducts(productsArray: json as! [[String: AnyObject]])
                let productsToRealm = self.products
                DispatchQueue.main.async {
                    autoreleasepool {
                        self.updateRealm(productsFromServer: productsToRealm)
                        self.productsTableView.reloadData()
                    }
                }
//                DispatchQueue.main.async {
//                    self.productsTableView.reloadData()
//                }
                
            } catch let error as NSError {
                print("Another error:")
                print(error)
            }
        }).resume()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if let t = cell?.textLabel?.text {
            print(t)
            performSegue(withIdentifier: "segueToProduct", sender: self)
        }
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "segueToProduct") {
            let destinationController = segue.destination as! ProductViewController
            destinationController.product = products[self.productsTableView.indexPathForSelectedRow!.row]
        }
    }
}

