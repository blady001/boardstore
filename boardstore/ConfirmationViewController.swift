//
//  ConfirmationViewController.swift
//  boardstore
//
//  Created by MacBlady on 10.06.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit
import RealmSwift

class ConfirmationViewController: UIViewController {

    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var itemsTextView: UITextView!
    @IBOutlet weak var labelTotal: UILabel!
    
    var order: Order?
    
    let postUrl = "http://localhost:8080/order"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        addressTextView.text = order!.address
        setupItemsTextView()
        labelTotal.text = "Total: \(order!.totalAmount)$"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Buy", style: .plain, target: self, action: #selector(confirmOrder))
    }
    
    
    func confirmOrder() {
        let dict = order!.toDictionary()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            
//            if let jsonStr = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
//                print(jsonStr)
//            } else {
//                print("Couldnt convert json to string")
//            }
            
            var request = URLRequest(url: URL(string: self.postUrl)!)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("NETWORKING ERROR=\(String(describing: error))")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    // TODO: set caritems + save order and cartitems + display message
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(String(describing: responseString))")
                self.createOrderAndUpdateCartItems()
            }
            task.resume()
            
            showPopup()
        } catch {
            print("ERROR WHILE READING CONVERTED DICT")
        }
    }
    
    func showPopup() {
        let alertController = UIAlertController(title: "Congratulations!", message: "You've just made an order!", preferredStyle: UIAlertControllerStyle.alert)
        
        let confirmationAction = UIAlertAction(title: "OK", style: .default) { (_) in
            let viewController = self.storyboard?.instantiateInitialViewController()!
            self.present(viewController!, animated: true, completion: nil)
        }
        
        alertController.addAction(confirmationAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func createOrderAndUpdateCartItems() {
        DispatchQueue.main.async {
            let realm = try! Realm()
            try! realm.write {
                for cartItem in self.order!.cartItems {
                    cartItem.order = self.order!
                }
                realm.add(self.order!)
            }
            print("Added order + updated cartitems")
        }
    }
    
    func setupItemsTextView() {
        var value = ""
        for item in self.order!.cartItems {
            let txt = "\(item.product!.name) - \(item.product!.price)$ x\(item.quantity)\n"
            value = value + txt
        }
        itemsTextView.text = value
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
