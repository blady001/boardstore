//
//  CartViewController.swift
//  boardstore
//
//  Created by MacBlady on 07.06.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit
import RealmSwift

class CartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var cartItemsTableView: UITableView!
    
    var cartItems = [CartItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Checkout", style: .plain, target: self, action: #selector(checkoutHandler))
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadCartItems()
    }
    
    func loadCartItems() {
        cartItems.removeAll()
        DispatchQueue.main.async {
            let realm = try! Realm()
            let foundCartItems = realm.objects(CartItem.self).filter("order == nil")
            for ci in foundCartItems {
                self.cartItems.append(ci)
            }
            self.cartItemsTableView.reloadData()
            print("Loaded cart items amount: \(foundCartItems.count)")
        }
    }
    
    func checkoutHandler() {
        if (cartItems.count == 0) {
            let alertController = UIAlertController(title: "Empty cart", message: "Add some items before checking out.", preferredStyle: UIAlertControllerStyle.alert)
            
            let confirmationAction = UIAlertAction(title: "OK", style: .default) { (_) in
                print("Ok clicked")
            }
            
            alertController.addAction(confirmationAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            performSegue(withIdentifier: "segueToOrderData", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "segueToOrderData") {
            let destinationController = segue.destination as! OrderDataViewController
            destinationController.cartItems = self.cartItems
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCartItem", for: indexPath)
        let item = cartItems[indexPath.row]
        cell.textLabel?.text = item.product!.name
        cell.detailTextLabel?.text = "Quantity: \(item.quantity)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.removeCartItem(index: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        }
    }
    
    func removeCartItem(index: Int) {
        let item = self.cartItems.remove(at: index)
        DispatchQueue.main.async {
            let realm = try! Realm()
            try! realm.write {
                realm.delete(item)
                print("Deleted item from realm")
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
