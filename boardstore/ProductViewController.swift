//
//  ProductViewController.swift
//  boardstore
//
//  Created by MacBlady on 30.04.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit
import RealmSwift

class ProductViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    var product: Product?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add to cart", style: .plain, target: self, action: #selector(addToCart))
        
        loadImage(imageUrlString: product!.imageUrl)
        titleLabel.text = product!.name
        priceLabel.text = "\(product!.price)$"
        descriptionTextView.text = product!.productDescription
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createCartItem(quantity: Int) {
        DispatchQueue.main.async {
            let cartItem = CartItem()
            cartItem.product = self.product!
            cartItem.quantity = quantity
            let realm = try! Realm()
            try! realm.write {
                realm.add(cartItem)
            }
        }
    }
    
    func addToCart() {
//        let alertController = UIAlertController(title: "Email?", message: "Please input your email:", preferredStyle: .Alert)
//        
//        let confirmAction = UIAlertAction(title: "Confirm", style: .Default) { (_) in
//            if let field = alertController.textFields![0] as? UITextField {
//                // store your data
//                NSUserDefaults.standardUserDefaults().setObject(field.text, forKey: "userEmail")
//                NSUserDefaults.standardUserDefaults().synchronize()
//            } else {
//                // user did not fill field
//            }
//        }
//        
//        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
//        
//        alertController.addTextFieldWithConfigurationHandler { (textField) in
//            textField.placeholder = "Email"
//        }
//        
//        alertController.addAction(confirmAction)
//        alertController.addAction(cancelAction)
//        
//        self.presentViewController(alertController, animated: true, completion: nil)
        
//        createCartItem()
        let alertController = UIAlertController(title: "Adding item", message: "How many items would you like to order?", preferredStyle: UIAlertControllerStyle.alert)
        
        let confirmationAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let quantity = Int(alertController.textFields![0].text!) {
                // store data
//                let quantity = Int(text)!
                print("Quantity entered: \(quantity)")
                self.createCartItem(quantity: quantity)
                let viewController = self.storyboard?.instantiateInitialViewController()!
                self.present(viewController!, animated: true, completion: nil)
            } else {
                // warning
                print("No value provided!")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            print("Cancel selected!")
        }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter integer number"
        }
        
        alertController.addAction(confirmationAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
//        alertController.addTextField { (textField) in
//            textField.text = ""
//        }
//        
//        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { [weak alertController] (_) in
//            print("OK pressed")
//            let quantity = Int(alertController!.textFields![0].text!)
//            createCartItem(quantity: quantity!)
//        })
//        alertController.addAction(okAction)
//        self.present(alertController, animated: true, completion: nil)
    }
    
    func loadImage(imageUrlString: String) {
        print("Loading image form: \(imageUrlString)")
        let pictureUrlObj = URL(string: imageUrlString)!
        
        // Creating a session object with the default configuration.
        // You can read more about it here https://developer.apple.com/reference/foundation/urlsessionconfiguration
        let session = URLSession(configuration: .default)
        
        // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
        let downloadPicTask = session.dataTask(with: pictureUrlObj) { (data, response, error) in
            // The download has finished.
            if let e = error {
                print("Error downloading cat picture: \(e)")
            } else {
                // No errors found.
                // It would be weird if we didn't have a response, so check for that too.
                if let res = response as? HTTPURLResponse {
                    print("Downloaded picture with response code \(res.statusCode)")
                    if let imageData = data {
                        // Finally convert that Data into an image and do what you wish with it
                        // Do something with your image.
                        let image = UIImage(data: imageData)
                        DispatchQueue.main.async {
                            self.imageView.image = image
                        }
                    } else {
                        print("Couldn't get image: Image is nil")
                    }
                } else {
                    print("Couldn't get response code for some reason")
                }
            }
        }
        downloadPicTask.resume()
    }
        

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
