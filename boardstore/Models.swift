//
//  Models.swift
//  boardstore
//
//  Created by MacBlady on 29.04.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import RealmSwift
import Foundation

extension Object {
    func toDictionary() -> NSDictionary {
        let properties = self.objectSchema.properties.map { $0.name }
        let dictionary = self.dictionaryWithValues(forKeys: properties)
        let mutabledic = NSMutableDictionary()
        mutabledic.setValuesForKeys(dictionary)
        
        for prop in self.objectSchema.properties as [Property]! {
            // find lists
            if let nestedObject = self[prop.name] as? Object {
                mutabledic.setValue(nestedObject.toDictionary(), forKey: prop.name)
            } else if let nestedListObject = self[prop.name] as? ListBase {
                var objects = [AnyObject]()
                for index in 0..<nestedListObject._rlmArray.count  {
                    let object = nestedListObject._rlmArray[index] as AnyObject
                    objects.append(object.toDictionary())
                }
                mutabledic.setObject(objects, forKey: prop.name as NSCopying)
            }
        }
        return mutabledic
    }
}

class User: Object {
    dynamic var username = ""
    dynamic var firstname = ""
    dynamic var lastname = ""
    dynamic var password = ""
    dynamic var email = ""
    dynamic var address = ""
}

class Product: Object {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var productDescription = ""
    dynamic var price = ""
    dynamic var amount = 0
    dynamic var category = ""
    dynamic var imageUrl = ""
}

class CartItem: Object {
    dynamic var product: Product?
    dynamic var order: Order?
    dynamic var quantity = 1
}

class Order: Object {
    dynamic var state = ""
    let cartItems = List<CartItem>()
    dynamic var address = ""
    dynamic var addresseeName = ""
    dynamic var paymentMethod = ""
    dynamic var totalAmount = ""
}

class Category: Object {
    var name = ""
    var categoryDescription = ""
}
