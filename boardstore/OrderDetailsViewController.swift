//
//  OrderDetailsViewController.swift
//  boardstore
//
//  Created by MacBlady on 10.06.2017.
//  Copyright © 2017 MacBlady. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController {
    
    var order: Order?

    @IBOutlet weak var detailsTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Order details"
        fillDetailsTextView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillDetailsTextView() {
        var value = "Products ordered:\n\n"
        for item in self.order!.cartItems {
            let txt = "\(item.product!.name) - \(item.product!.price)$ x\(item.quantity)\n"
            value = value + txt
        }
        
        value = value + "\nTotal: \(order!.totalAmount)$\n"
        
        value = value + "\nAddress info:\n"
        value = value + order!.address
        
        self.detailsTextView.text = value
    }

}
